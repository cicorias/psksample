CFLAGS=-I./openssl/include  -DOPENSSL_NO_ENGINE -Wall -Wextra
all: psk_client psk_server
psk_client: psk_client.c
	gcc $(CFLAGS) -o $@ $^ -lcrypto -lssl -L./openssl
psk_server:psk_server.c #s_cb.c 
	gcc $(CFLAGS) -o $@ $^ -lcrypto -lssl -L./openssl
clean:
	rm -f psk_server psk_client
